import request from "@/utils/request.js";
 
export function getGoods()
{
  return request.get("points/list",{},{ noAuth : true});
}
export function getPickuPoint()
{
  return request.get("points/my",{},{ noAuth : true});
}
export function setPickuPoint(pointId)
{
  return request.get(`points/choose/${pointId}`,{},{ noAuth : true});
}