

import request from "@/utils/request.js";


export function makeDish(){
  return request.get(`step/video`,{},{noAuth:true});
}



export function searchDish(name){
  return request.get(`step/search/${name}`,{},{noAuth:true});
}