import { defineConfig, presetUno,transformerVariantGroup } from "unocss"
export default defineConfig({
  // ...UnoCSS options
  presets: [presetUno()],
  transformers: [transformerVariantGroup()],
})
