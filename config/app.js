// +----------------------------------------------------------------------
// | CRMEB [ CRMEB赋能开发者，助力企业发展 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2024 https://www.crmeb.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed CRMEB并不是自由软件，未经许可不能去掉CRMEB相关版权
// +----------------------------------------------------------------------
// | Author: CRMEB Team <admin@crmeb.com>
// +----------------------------------------------------------------------
//移动端商城API 网络接口修改此配置，小程序域名要求https 例如：https://api.front.merchant.java.crmeb.net

// let domain = 'http://192.168.10.110:20810'
// let domain = 'http://192.168.10.108:20910'
// let domain = 'https://www.jiabaineng.com:20710'

// let domain = 'http://192.168.10.109:20710'
// let domain = 'http://192.168.10.108:20910'
// let domain = 'http://192.168.10.110:20810'
//进哥服务器
let domain = 'https://www.jiabaineng.com:20710'

// let domain = 'https://www.crmeb.com/index/java_merchant'

module.exports = {
	// 请求域名 格式： https://您的域名
	// #ifdef MP || APP-PLUS
		HTTP_REQUEST_URL: domain,
		// H5商城地址
		// HTTP_H5_URL: 'https://app',
	// #endif
	// HTTP_ADMIN_URL:'http://192.168.10.109:8080',   //PC后台的API请求地址，上传图片用
	// #ifdef H5
		HTTP_REQUEST_URL:domain,
	// #endif
	HEADER:{
		'content-type': 'application/json'
	},
	HEADERPARAMS:{
		'content-type': 'application/x-www-form-urlencoded'
	},
	// 回话密钥名称 请勿修改此配置
	TOKENNAME: 'Authori-zation',
	// 缓存时间 0 永久
	EXPIRE:0,
	//分页最多显示条数
	LIMIT: 10
};
